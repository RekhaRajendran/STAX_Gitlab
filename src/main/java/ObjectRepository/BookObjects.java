package ObjectRepository;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public interface BookObjects {
	
		
	public String ClickBookNow();
	
	public void ClickContinue();
	
	public void selectPayDepositAmount();
	
	public void selectPayFullAmount();
	
	public void EnterPassengerDetails();
	
	public void EnterValidCardDetailsStaging();
	
	public void EnterInvalidCardDetails();
	
	public void EnterBillingDetails();
	
	public void AcceptBookingTerms();
	
	public void ClickBookAtCheckOut();
	
	public void ClickHoldRequest();
	
	public String GetPriceFromBookingPage();
	
	public void ClickNeedMoreTimeRB();
	
	public void AdjustDepartureDateForHold();
	
	public void AdjustDepartureDateForDeposit();
	
	public void AdjustDepartureDateForFullPayment();
	
	public void VerifyFullPaymentBooking();
	
	public void VerifyHoldBooking();
	
	public void VerifyDepositBooking();
	
	public void VerifyInvalidPayment();

}
