package ObjectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public interface EmailObjects {
	
	/*
	 * public final By EmailUsButton =
	 * By.xpath("//a[@class='emailPopUp btn-sta btn-email']");
	 * 
	 * public final By EmailUs_FirstName = By.id("txteffirstname");
	 * 
	 * public final By EmailUs_LastName = By.id("txteflastname");
	 * 
	 * public final By EmailUs_EmailId = By.id("txtefemail");
	 * 
	 * public final By EmailUs_PhoneNo = By.id("txtefphone");
	 * 
	 * public final By EmailUs_SendMyEmail = By.id("btnSubmit");
	 * 
	 * public final By EmailUs_Close = By.xpath("//div/a[contains(.,'Close')]");
	 */
	
	public void ClickEmailUs() throws Exception;

	public void EnterEmailDetails() throws Exception;

	public void ClickSendMyEmail() throws Exception;

	public void VerifyEmailSentText();

	public void CloseEmailPopup() throws Exception;

}
