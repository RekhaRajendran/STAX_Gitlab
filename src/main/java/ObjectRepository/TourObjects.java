package ObjectRepository;

public interface TourObjects {

	public String SelectRandomTour();
	
	public void SelectLowestPriceTour() throws Exception;
	
	public void SelectHighestPriceTour() throws Exception;
	
	public String GetPriceFromTripDetailsPage();
}
