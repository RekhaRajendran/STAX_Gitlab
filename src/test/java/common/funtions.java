package common;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import io.codearte.jfairy.Fairy;
import io.codearte.jfairy.producer.person.Person;

public class funtions {

	public Fairy fairy = Fairy.create();
	public Person person = fairy.person();

	public static String getTourLinkwhichHasDeposit(WebDriver driver) throws InterruptedException {
		String parentWindow = driver.getWindowHandle();

		List<WebElement> list = driver.findElements(By.xpath("//*[@ng-href]"));
		System.out.println("Total windows will be opened" + list.size());
		String tourwithdeposit = null;

		for (WebElement e : list) {
			String link = e.getAttribute("href");
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("arguments[0].click();", e);

		}

		Set<String> winHandles = driver.getWindowHandles();
		for (String handle : winHandles) {

			driver.switchTo().window(handle);

			Thread.sleep(1000);
			if (driver.findElements(By.xpath("//img[@class='b-deposit ']")).size() > 0) {
				tourwithdeposit = driver.getCurrentUrl();
				System.out.println(driver.getCurrentUrl());

				break;
			}
		}
		return tourwithdeposit;

	}
	
	public static String getScreenshot(WebDriver CBEDriver, String ScreenShotName) throws IOException
    {
        String dateName=new SimpleDateFormat("YYYYMMDDHHMMSS").format(new Date());
        TakesScreenshot ts=(TakesScreenshot)CBEDriver;
        File source=ts.getScreenshotAs(OutputType.FILE);
        String destination=System.getProperty("user.dir")+"/FailedScreenshots/"+ScreenShotName+dateName+".png";
        File finalDestination=new File(destination);
        FileUtils.copyFile(source, finalDestination);

        String Imagepath="file://Machinename/FailedScreenshots/"+ScreenShotName+dateName+".png";
        return Imagepath;
    }

	public static String screenshot(WebDriver driver, long ms) throws IOException {

		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(scrFile, new File("./Reports/Screenshots_Fail/" + ms + ".png"));

		File filepath = new File("Reports/Screenshots_Fail/" + ms + ".png");
		String dest1 = filepath.getAbsolutePath();
		String dest = "Screenshots_Fail/" + ms + ".png";
		File destination = new File(dest);

		System.out.println("ScreenShot Taken");

		return dest;

	}

	public void Dropdown(WebElement ele, String text) throws InterruptedException {
		ele.click();
		Thread.sleep(4000);
		Select dropdown = new Select(ele);
		dropdown.deselectByVisibleText(text);

	}

}
