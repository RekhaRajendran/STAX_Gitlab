Feature: Verify Email Us functionality 

@UK
Scenario Outline: Verify email us functionality for UK <Supplier>
Given User Launches UK <Supplier> Application
Given User Selects a lowest price tour
And Clicks on Email us
And Provides all the details
And Click on Send My Email
Then An email will be sent

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|



@AU
Scenario Outline: Verify email us functionality for AU <Supplier>
	Given User Launches AU <Supplier> Application
	Given User Selects a lowest price tour
	And Clicks on Email us
	And Provides all the details
	And Click on Send My Email
	Then An email will be sent

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|

@NZ
Scenario Outline: Verify email us functionality for NZ <Supplier>
	Given User Launches NZ <Supplier> Application
	Given User Selects a lowest price tour
	And Clicks on Email us
	And Provides all the details
	And Click on Send My Email
	Then An email will be sent

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|


@US
Scenario Outline: Verify email us functionality for US <Supplier>
	Given User Launches US <Supplier> Application
	Given User Selects a lowest price tour
	And Clicks on Email us
	And Provides all the details
	And Click on Send My Email
	Then An email will be sent

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|