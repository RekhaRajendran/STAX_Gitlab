Feature: Verify whether the User is able to Make Hold Bookings

@UK
Scenario: Verify whether the User is able to make a hold booking
Given User Launches UK GAdventures Application
And Selects the departure month as three months from now in filters
Given User Selects a random tour
And Switches to New Window
And User clicks on Book Now
And Adjust Departure Date for Hold Booking
And Selects the Need more time RB
And Click On Continue
And Fills the passenger details
And Accepts the Terms and Conditions
And Clicks On Hold Request
Then Hold Booking should be created

@US
Scenario: Verify whether the User is able to make a hold booking
Given User Launches US GAdventures Application
And Selects the departure month as three months from now in filters
Given User Selects a random tour
And Switches to New Window
And User clicks on Book Now
And Adjust Departure Date for Hold Booking
And Selects the Need more time RB
And Click On Continue
And Fills the passenger details
And Accepts the Terms and Conditions
And Clicks On Hold Request
Then Hold Booking should be created

@AU
Scenario: Verify whether the User is able to make a hold booking
Given User Launches AU GAdventures Application
And Selects the departure month as three months from now in filters
Given User Selects a random tour
And Switches to New Window
And User clicks on Book Now
And Adjust Departure Date for Hold Booking
And Selects the Need more time RB
And Click On Continue
And Fills the passenger details
And Accepts the Terms and Conditions
And Clicks On Hold Request
Then Hold Booking should be created

@NZ
Scenario: Verify whether the User is able to make a hold booking
Given User Launches NZ GAdventures Application
And Selects the departure month as three months from now in filters
Given User Selects a random tour
And Switches to New Window
And User clicks on Book Now
And Adjust Departure Date for Hold Booking
And Selects the Need more time RB
And Click On Continue
And Fills the passenger details
And Accepts the Terms and Conditions
And Clicks On Hold Request
Then Hold Booking should be created