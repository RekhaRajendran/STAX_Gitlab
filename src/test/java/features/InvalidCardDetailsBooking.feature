Feature: Verify whether the User is able to Make Full Payment Bookings

@UK
Scenario: Verify whether the User is able to make a Full Payment booking
Given User Launches UK GAdventures Application
And Selects the departure month as three months from now in filters
Given User Selects a random tour
And Switches to New Window
And User clicks on Book Now
And Adjust Departure date for Full Payment Booking
And Selects the Pay Full Amount Option
And Click On Continue
And Fills the passenger details
And Fills the Invalid Card Details
And Fills the Billing Details
And Accepts the Terms and Conditions
And Complete The Booking
Then Payment Error Popup should be displayed