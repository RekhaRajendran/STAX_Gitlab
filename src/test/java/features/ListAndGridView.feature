Feature: Verify Email Us List View and Grid view functionality 

@UK
Scenario Outline: Verify List View and Grid view functionality for UK <Supplier>
Given User Launches UK <Supplier> Application
And User clicks on List View
Then Tours should be displayed in List view
And User clicks on Grid view
Then Tours should be displayed in Grid view

Examples:
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|