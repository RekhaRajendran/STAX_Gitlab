Feature: User Verifies the Sort Order Functionality 

@UK
Scenario Outline:Verify if Sort order is displayed properly for <Supplier>
Given User Launches UK <Supplier> Application
And User selects the PriceLowest 
Then Verify the Results for PriceLowest
And User selects the PriceHigest 
Then Verify the Results for PriceHigest
And User selects the PricePerDayLowest
Then Verify the Results for PricePerDayLowest
And User selects the PricePerDayHigest 
Then Verify the Results for PricePerDayHigest
And User selects the DurationLowest 
Then Verify the Results for DurationLowest
 
	
Examples: 
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|
		
@US
Scenario Outline: Verify if Sort order of <Sort Option> for <Supplier> 
Given User Launches US <Supplier> Application 
And User selects the PriceLowest 
Then Verify the Results for PriceLowest
And User selects the PriceHigest 
Then Verify the Results for PriceHigest
And User selects the PricePerDayLowest
Then Verify the Results for PricePerDayLowest
And User selects the PricePerDayHigest 
Then Verify the Results for PricePerDayHigest
And User selects the DurationLowest 
Then Verify the Results for DurationLowest
 
	
Examples: 
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|


@AU
Scenario Outline: Verify if Sort order is displayed properly <Sort Option> for <Supplier> 
Given User Launches AU <Supplier> Application 
And User selects the PriceLowest 
Then Verify the Results for PriceLowest
And User selects the PriceHigest 
Then Verify the Results for PriceHigest
And User selects the PricePerDayLowest
Then Verify the Results for PricePerDayLowest
And User selects the PricePerDayHigest 
Then Verify the Results for PricePerDayHigest
And User selects the DurationLowest 
Then Verify the Results for DurationLowest
 
	
Examples: 
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|
		
		
@NZ
Scenario Outline: Verify if the Sort order is displayed properly <Sort Option> for <Supplier> 
Given User Launches NZ <Supplier> Application 
And User selects the PriceLowest 
Then Verify the Results for PriceLowest
And User selects the PriceHigest 
Then Verify the Results for PriceHigest
And User selects the PricePerDayLowest
Then Verify the Results for PricePerDayLowest
And User selects the PricePerDayHigest 
Then Verify the Results for PricePerDayHigest
And User selects the DurationLowest 
Then Verify the Results for DurationLowest
 
	
Examples: 
|Supplier|
|Contiki|
|GAdventures|
|1Source|
|Dragoman|
|GrandAmerica|
|TrekAmerica|
|Radical|