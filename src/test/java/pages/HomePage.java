package pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.vimalselvam.cucumber.listener.Reporter;

import common.funtions;

public class HomePage {

	public HomePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	funtions commonfunctions = new funtions();

	@FindBy(how = How.XPATH, using = "//li[@class='account nav-dropdown']")
	public WebElement Signinbutton;

	@FindBy(how = How.XPATH, using = "//button[@id='sta-cookie-save-all-button']")
	public List<WebElement> AcceptCookies;
	@FindBy(how = How.XPATH, using = "//button[@id='sta-cookie-save-all-button']")
	public WebElement Acceptookiesbuuton;

	@FindBy(how = How.XPATH, using = "//span[@class='trip-price-inner ng-binding']")
	public List<WebElement> priceXpath;
	@FindBy(how = How.XPATH, using = "//span[@class='trip-days ng-binding']")
	public WebElement daysXpath;

	@FindBy(how = How.XPATH, using = "//span[@class='ng-binding']")
	public WebElement TourCountInLabel;

	@FindBy(how = How.ID, using = "sortSelect")
	public WebElement SortDropDown;

	public void Sortby(String text) throws InterruptedException {
		SortDropDown.click();
		Thread.sleep(4000);
		Select dropdown = new Select(SortDropDown);
		dropdown.selectByValue(text);
		Thread.sleep(7000);
		getpriceorder(text);

	}

	public void SortbyHighprice(String text) throws InterruptedException {
		SortDropDown.click();
		Thread.sleep(4000);
		Select dropdown = new Select(SortDropDown);
		dropdown.selectByValue(text);
		Thread.sleep(7000);


	}



	public void getpriceorder(String ordertype) {

		List<String> currentprice = new ArrayList<>();

		List<String> oldprice = new ArrayList<>();
		List<WebElement> price = priceXpath;
		for (WebElement match : price) {
			currentprice.add(match.getText());
			oldprice.add(match.getText());
			System.out.println(match.getText());

		}

		if (ordertype.equals("PriceHigest")) {

			System.out.println("Now in Price high");
			Reporter.addStepLog("Actual Value" + oldprice);
			Collections.sort(currentprice, Collections.reverseOrder());

			Object[] fishSorted = currentprice.toArray();
			List expectedprice = Arrays.asList(fishSorted);

			Reporter.addStepLog("Expected Value" + expectedprice);
			Assert.assertEquals(oldprice, expectedprice);
		} else if (ordertype.equals("PriceLowest")) {
			System.out.println("Now in Price low");

			Reporter.addStepLog("Actual Value" + oldprice);

			Collections.sort(currentprice, new Comparator<String>() {

				@Override
				public int compare(String o1, String o2) {
					return extractInt(o1) - extractInt(o2);
				}

				int extractInt(String s) {
					String num = s.replaceAll("\\D", "");
					// return 0 if no digits found
					return num.isEmpty() ? 0 : Integer.parseInt(num);
				}
			});

			Reporter.addStepLog("Expected Value" + currentprice);
			Assert.assertEquals(oldprice, currentprice);
		}

	}

	public void Clicksignin() {
		Signinbutton.click();
	}

	public void Click_AcceptCookies() {
		Acceptookiesbuuton.click();
	}

}
