package pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import common.funtions;

public class ToursPage {

	public ToursPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//input[@class='form-control input-number']")
	public WebElement NumberofAdults;
	@FindBy(how = How.ID, using = "ddlAGender_0")
	public WebElement Gender;

	@FindBy(how = How.XPATH, using = "//a[@class='btn-sta btn-primary btn-book']")
	public WebElement Booknow;
	@FindBy(how = How.XPATH, using = "//span[@class='downArrow']")
	public WebElement Downarrow;
	@FindBy(how = How.XPATH, using = "//a[@class='btn btn-common']")
	public WebElement ContinueButton;

	@FindBy(how = How.ID, using = "PaymentDueNowRow")
	public WebElement Paymentduenow;
	@FindBy(how = How.ID, using = "PaymentDueLaterRow")
	public WebElement Paymentduelater;
	@FindBy(how = How.ID, using = "summary-body-mob")
	public WebElement TripSummary;


	@FindBy(how = How.ID, using = "txtAFirstName_0")
	public WebElement FirstName;
	@FindBy(how = How.ID, using = "txtALastName_0")
	public WebElement LasttName;
	@FindBy(how = How.ID, using = "txtAMiddleName_0")
	public WebElement MiddletName;
	@FindBy(how = How.ID, using = "divddlDOBADate_0")
	public WebElement DOBday;
	@FindBy(how = How.ID, using = "divddlDOBAMonth_0")
	public WebElement DOBMonth;
	@FindBy(how = How.ID, using = "divddlDOBAYear_0")
	public WebElement DOByear;

	@FindBy(how = How.ID, using = "ddlANationalities_0")
	public WebElement Nationality;
	@FindBy(how = How.ID, using = "txtPhoneNo")
	public WebElement Phonenumber;
	@FindBy(how = How.ID, using = "txtEmailAddress")
	public WebElement Emailaddress;
	@FindBy(how = How.ID, using = "chkCancellationPolicy")
	public WebElement SuppilerTerms;
	@FindBy(how = How.ID, using = "chkTermsofService")
	public WebElement STAterms;
	@FindBy(how = How.XPATH, using = "//input[@class='btn-green']")
	public WebElement HoldRequestButton;

	@FindBy(how = How.XPATH, using = "//a[@class='btn-green btn-letsGo']")
	public WebElement ConfirmtourNow;
	@FindBy(how = How.XPATH, using = "//span[@class='booking-col3']")
	public WebElement DepositAmout;
	@FindBy(how = How.XPATH, using = "//div[@class='centeredCell ng-binding'] ")
	public List<WebElement> TourdepartureYear;
	@FindBy(how = How.XPATH, using = "//input[@class='opt-sp-note']")
	public WebElement NeedmoretimetodecideRadiobutton;
	funtions commonfunctions = new funtions();



}
