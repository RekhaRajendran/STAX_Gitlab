Feature: Verify whether the User is able to Make Deposit Bookings

@UK
Scenario: Verify whether the User is able to make a Deposit booking
Given User Launches UK GAdventures Application
And Selects the departure month as three months from now in filters
Given User Selects a random tour
And Switches to New Window
And User clicks on Book Now
And Adjust Departure date for Deposit Booking
And Selects the Pay Deposit Option
And Click On Continue
And Fills the passenger details
And Fills the Card Details In Staging
And Fills the Billing Details
And Accepts the Terms and Conditions
And Complete The Booking
Then Deposit Booking Should be Done

@US
Scenario: Verify whether the User is able to make a Deposit booking
Given User Launches US GAdventures Application
And Selects the departure month as three months from now in filters
Given User Selects a random tour
And Switches to New Window
And User clicks on Book Now
And Adjust Departure date for Deposit Booking
And Selects the Pay Deposit Option
And Click On Continue
And Fills the passenger details
And Fills the Card Details In Staging
And Fills the Billing Details
And Accepts the Terms and Conditions
And Complete The Booking
Then Deposit Booking Should be Done

@AU
Scenario: Verify whether the User is able to make a Deposit booking
Given User Launches AU GAdventures Application
And Selects the departure month as three months from now in filters
Given User Selects a random tour
And Switches to New Window
And User clicks on Book Now
And Adjust Departure date for Deposit Booking
And Selects the Pay Deposit Option
And Click On Continue
And Fills the passenger details
And Fills the Card Details In Staging
And Fills the Billing Details
And Accepts the Terms and Conditions
And Complete The Booking
Then Deposit Booking Should be Done

@NZ
Scenario: Verify whether the User is able to make a Deposit booking
Given User Launches NZ GAdventures Application
And Selects the departure month as three months from now in filters
Given User Selects a random tour
And Switches to New Window
And User clicks on Book Now
And Adjust Departure date for Deposit Booking
And Selects the Pay Deposit Option
And Click On Continue
And Fills the passenger details
And Fills the Card Details In Staging
And Fills the Billing Details
And Accepts the Terms and Conditions
And Complete The Booking
Then Deposit Booking Should be Done