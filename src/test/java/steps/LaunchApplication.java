package steps;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.gherkin.model.Feature;
import com.aventstack.extentreports.gherkin.model.IGherkinFormatterModel;
import com.aventstack.extentreports.gherkin.model.Scenario;

import Base.BaseUtil;
import CodeImplementation.Basic;
import CodeImplementation.Booking;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class LaunchApplication extends BaseUtil {
	Basic basic = new Basic();
	Booking book = new Booking(Driver);
	
	@And("^Switches to New Window$")
	@Test(description ="switches_to_new_window")
	public void switches_to_new_window() throws Throwable {
		
		
		basic.SwitchToNewWindow();
		Thread.sleep(5000);
	}

	@Test(description = "user_launches_uk_application")
	@Given("^User Launches UK (.+) Application$")
	public void user_launches_uk_application(String supplier) throws Throwable {
		String Supplier = supplier;
		System.out.println("Application Launch");
		InputStream input = new FileInputStream("UtilitiesProd.properties");
		Properties prop = new Properties();
		prop.load(input);

		if (Supplier.equalsIgnoreCase("Contiki")) {
			Driver.navigate().to(prop.getProperty("Contiki_uk"));
		}

		if (Supplier.equalsIgnoreCase("GAdventures")) {
			Driver.navigate().to(prop.getProperty("GAdventures_uk"));
		}

		if (Supplier.equalsIgnoreCase("Dragoman")) {
			Driver.navigate().to(prop.getProperty("Dragoman_uk"));
		}

		if (Supplier.equalsIgnoreCase("1Source")) {
			Driver.navigate().to(prop.getProperty("1Source_uk"));
		}
		if (Supplier.equalsIgnoreCase("TrekAmerica")) {
			Driver.navigate().to(prop.getProperty("TrekAmerica_uk"));
		}
		if (Supplier.equalsIgnoreCase("GrandAmerica")) {
			Driver.navigate().to(prop.getProperty("GrandAmerica_uk"));
		}
		if (Supplier.equalsIgnoreCase("Radical")) {
			Driver.navigate().to(prop.getProperty("Radical_uk"));
		}

		Thread.sleep(5000);
		basic.AcceptCookies();
		Thread.sleep(2000);
	}

	@Given("^User Launches AU (.+) Application$")
	@Test(description ="user_launches_au_application")
	public void user_launches_au_application(String supplier) throws Throwable {		
		
		basic.LaunchAUURL(supplier);
		Thread.sleep(6000);
		basic.AcceptCookies();
		Thread.sleep(2000);
	}

	@Given("^User Launches NZ (.+) Application$")
	@Test(description ="user_launches_nz_application")
	public void user_launches_nz_application(String supplier) throws Throwable {
		
		
		basic.LaunchNZURL(supplier);
		Thread.sleep(6000);
		basic.AcceptCookies();
		Thread.sleep(2000);
	}

	@Given("^User Launches US (.+) Application$")
	public void user_launches_us_application(String supplier) throws Throwable {
		
		
		basic.LaunchUSURL(supplier);
		Thread.sleep(6000);
		basic.AcceptCookies();
		Thread.sleep(2000);
	}
}
