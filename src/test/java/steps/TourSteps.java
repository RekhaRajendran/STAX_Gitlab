package steps;

import org.testng.Assert;

import Base.BaseUtil;
import CodeImplementation.Basic;
import CodeImplementation.Booking;
import CodeImplementation.Home;
import CodeImplementation.Tour;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;



public class TourSteps extends BaseUtil {

	Tour tour = new Tour(Driver);
	Booking book = new Booking(Driver);
	int Price_SearchPage;
	int Price_TourPage;
	int Price_BookingPage;
	Basic basic = new Basic();
	Home Home = new Home(Driver);

	@Given("^User Selects a random tour$")
	public void user_selects_a_random_tour() throws Throwable {
		Price_SearchPage = Integer.parseInt(tour.SelectRandomTour().replaceAll("[^0-9]", ""));
	}

	@Given("^User Selects a Highest price tour$")
    public void user_selects_a_highest_price_tour() throws Throwable {
		tour.SelectHighestPriceTour();
    }
	
	@Then("^The Tour Price should be same in Tour Details Page$")
	public void the_tour_price_should_be_same_in_tour_details_page() throws Throwable {
		 basic.SwitchToNewWindow();
	        Thread.sleep(5000);
		Price_TourPage = Integer.parseInt(tour.GetPriceFromTripDetailsPage().replaceAll("[^0-9]", ""));
		Assert.assertTrue(Price_TourPage==Price_SearchPage);
	}

	@Then("^The Tour Price Should be same in Trip Summary$")
	public void the_tour_price_should_be_same_in_trip_summary() throws Throwable {
		Price_BookingPage=Integer.parseInt(book.GetPriceFromBookingPage().replaceAll("[^0-9]", ""));
		Assert.assertTrue(Price_BookingPage==Price_TourPage);
	}

}
